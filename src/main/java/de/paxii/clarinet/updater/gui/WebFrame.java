package de.paxii.clarinet.updater.gui;

import javax.swing.*;

import javafx.application.Platform;
import javafx.concurrent.Worker;
import javafx.embed.swing.JFXPanel;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebErrorEvent;
import javafx.scene.web.WebView;

/**
 * Created by Lars on 14.04.2017.
 */
public class WebFrame extends JFrame {

  private JFXPanel jfxPanel;
  private WebEngine webEngine;

  public WebFrame() {
    super();

    this.initComponents();
  }

  private void initComponents() {
    this.jfxPanel = new JFXPanel();
    this.add(this.jfxPanel);
    this.setupWebView();
  }

  private void setupWebView() {
    Platform.runLater(() -> {
      WebView webView = new WebView();
      this.webEngine = webView.getEngine();
      this.jfxPanel.setScene(new Scene(webView));
    });
  }

  public void registerListener(UrlChangeListener urlChangeListener) {
    Platform.runLater(() -> this.webEngine.locationProperty().addListener((observable, oldValue, newValue) ->
            urlChangeListener.change(oldValue, newValue)));
  }

  public void load(String url) {
    Platform.runLater(() -> this.webEngine.load(url));
  }

  public void loadContent(String content) {
    Platform.runLater(() -> this.webEngine.loadContent(content, "text/html"));
  }

  public void setErrorHandler(EventHandler<WebErrorEvent> eventHandler) {
    Platform.runLater(() -> this.webEngine.setOnError(eventHandler));
  }

  public Worker<Void> getLoadWorker() {
    return this.webEngine.getLoadWorker();
  }

}
