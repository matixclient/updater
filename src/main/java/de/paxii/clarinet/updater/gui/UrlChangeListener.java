package de.paxii.clarinet.updater.gui;

/**
 * Created by Lars on 14.04.2017.
 */
public interface UrlChangeListener {

  void change(String oldUrl, String newUrl);

}
