package de.paxii.clarinet.updater.util;

import java.io.File;

/**
 * Created by Lars on 14.04.2017.
 */
public class PathDetector {

  /**
   * Resolves the path to .minecraft based on OS
   * 60% of the time, it works everytime.
   *
   * @return String the Path to .minecraft
   */
  public static String detectPath() {
    String path = "";
    String osName = System.getProperty("os.name").toLowerCase();
    String userHome = System.getProperty("user.home");

    boolean isWindows = osName.contains("win");
    boolean isMac = osName.contains("mac");
    boolean isUnix = osName.contains("nix") || osName.contains("nux");

    if (isWindows) {
      path = System.getenv("APPDATA") + File.separator + ".minecraft" + File.separator;
    } else if (isMac) {
      path = userHome + File.separator + "Application Support" + File.separator + "minecraft" + File.separator;
    } else if (isUnix) {
      path = userHome + File.separator + "minecraft" + File.separator;
    }

    return path;
  }

}
