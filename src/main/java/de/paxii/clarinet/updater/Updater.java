package de.paxii.clarinet.updater;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import de.paxii.clarinet.updater.gui.WebFrame;
import de.paxii.clarinet.updater.launcher.LauncherProfile;
import de.paxii.clarinet.updater.launcher.LauncherProfileParser;
import de.paxii.clarinet.updater.launcher.LauncherProfiles;
import de.paxii.clarinet.updater.util.JsonFetcher;
import de.paxii.clarinet.updater.util.PathDetector;
import de.paxii.clarinet.updater.version.VersionList;
import de.paxii.clarinet.updater.version.VersionObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.swing.*;

/**
 * Created by Lars on 14.04.2017.
 */
public class Updater {

  private static WebFrame webFrame;

  private static final String BASE_URL = "http://paxii.de/Matix/updater/";
  private static final String URL_UPDATING = BASE_URL + "downloading.html";
  private static final String[] ALLOWED_URLS = new String[]{
          URL_UPDATING
  };

  public static void main(String[] args) {
    webFrame = new WebFrame();
    webFrame.setTitle("Matix Updater");
    webFrame.setBounds(0, 0, 800, 500);
    webFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    webFrame.setVisible(true);
    webFrame.setErrorHandler(event -> Updater.displayError(event.getException()));
    String minecraftPath = args.length >= 3 ? args[2] : PathDetector.detectPath();

    if (minecraftPath.length() == 0 || !(new File(minecraftPath).exists())) {
      JFileChooser fileChooser = new JFileChooser();
      fileChooser.setDialogTitle("Select .minecraft Folder");
      fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
      int value = fileChooser.showSaveDialog(webFrame);

      if (value == JFileChooser.APPROVE_OPTION) {
        minecraftPath = fileChooser.getSelectedFile().getAbsolutePath();
      }
    }

    String gameVersion = args.length > 0 ? args[0] : "";
    String downloadUrl = args.length > 0 ? args[1] : "";

    if (gameVersion.length() == 0) {
      VersionList versionList = JsonFetcher.fetchData(BASE_URL + "../versions.json", VersionList.class);
      if (versionList != null) {
        String[] gameVersions = versionList.getVersions().stream()
                .filter(version -> version.getUrl() != null)
                .map(VersionObject::getGameVersion)
                .toArray(String[]::new);
        gameVersion = (String) JOptionPane.showInputDialog(
                webFrame,
                "Minecraft Version", "Please select Game Version",
                JOptionPane.QUESTION_MESSAGE,
                null,
                gameVersions,
                gameVersions[0]
        );

        if (gameVersion == null) {
          System.exit(0);
        }

        VersionObject selectedVersion = versionList.getByGameVersion(gameVersion).orElse(null);
        if (selectedVersion != null) {
          downloadUrl = selectedVersion.getDirectUrl();
        }
      }
    }

    String releaseUrl = downloadUrl;
    String matixVersion = "Matix" + gameVersion;
    File minecraftFolder = new File(minecraftPath);
    File matixFolder = new File(minecraftPath, "versions" + File.separator + matixVersion);

    webFrame.setTitle("Matix Update " + gameVersion);
    webFrame.setTitle("Downloading...");
    webFrame.load(URL_UPDATING);

    new Thread(() -> Updater.updateVersion(releaseUrl, minecraftFolder, matixFolder, matixVersion)).start();
  }

  private static void updateVersion(String downloadUrl, File minecraftFolder, File matixFolder, String matixVersion) {
    try {
      URL fileLocation = new URL(downloadUrl);
      File downloadLocation = new File(matixFolder, downloadUrl.substring(downloadUrl.lastIndexOf("/")));
      downloadLocation.getParentFile().mkdir();
      InputStream inputStream = fileLocation.openStream();
      Files.copy(inputStream, Paths.get(downloadLocation.toURI()), StandardCopyOption.REPLACE_EXISTING);

      ZipFile zipFile = new ZipFile(downloadLocation);
      for (String fileExtension : new String[]{".jar", ".json"}) {
        new File(matixFolder, matixVersion + fileExtension).delete();
        ZipEntry zipEntry = zipFile.getEntry(matixVersion + "/" + matixVersion + fileExtension);
        String fileName = zipEntry.getName().substring(zipEntry.getName().lastIndexOf("/"));
        Files.copy(zipFile.getInputStream(zipEntry), Paths.get(new File(matixFolder, fileName).toURI()), StandardCopyOption.REPLACE_EXISTING);
      }
      zipFile.close();

      downloadLocation.delete();

      String message = matixVersion + " was installed to \".minecraft/versions/" + matixVersion + "\".";
      if (!Updater.addLauncherProfile(minecraftFolder, matixVersion)) {
        message = matixVersion + " could not automatically be added to your launcher profiles.";
      }
      JOptionPane.showMessageDialog(null, message);

      System.exit(0);
    } catch (Exception e) {
      Updater.displayError(e);
      e.printStackTrace();
    }
  }

  private static boolean addLauncherProfile(File minecraftFolder, String profileName) {
    File profileFile = new File(minecraftFolder, "launcher_profiles.json");
    File backupFile = new File(minecraftFolder, "launcher_profiles.json.backup");
    try {
      backupFile.delete();
      Files.copy(Paths.get(profileFile.toURI()), Paths.get(backupFile.toURI()));
    } catch (IOException e) {
      Updater.displayError(e);
      e.printStackTrace();
    }
    LauncherProfileParser profileParser = new LauncherProfileParser();
    LauncherProfiles launcherProfiles = profileParser.getLauncherProfiles(profileFile);

    LauncherProfile launcherProfile = new LauncherProfile();
    launcherProfile.setName(profileName);
    launcherProfile.setLastVersionId(profileName);
    launcherProfile.setJavaArgs("-Xmx1G -XX:+UseConcMarkSweepGC -XX:+CMSIncrementalMode -XX:-UseAdaptiveSizePolicy -Xmn128M");

    launcherProfiles.getProfiles().put(profileName, launcherProfile);
    launcherProfiles.setSelectedProfile(profileName);

    Gson gson = new GsonBuilder().setPrettyPrinting().create();
    String newProfileFileContents = gson.toJson(launcherProfiles);

    try {
      PrintWriter printWriter = new PrintWriter(new FileOutputStream(profileFile, false));
      printWriter.print(newProfileFileContents);
      printWriter.close();
      return true;
    } catch (Exception e) {
      Updater.displayError(e);
      e.printStackTrace();

      return false;
    }
  }

  private static boolean isUrlAllowed(String url) {
    return Arrays.stream(ALLOWED_URLS).anyMatch(allowedUrl -> allowedUrl.equalsIgnoreCase(url));
  }

  private static void displayError(Throwable exception) {
    StringWriter stringWriter = new StringWriter();
    PrintWriter printWriter = new PrintWriter(stringWriter);
    exception.printStackTrace(printWriter);

    webFrame.setTitle("Error");
    webFrame.loadContent(String.format("<html><head></head><body><h1>%s</h1><h2><strong>%s</strong></h2><p>%s</p></body></html>",
            "An error occurred",
            "If it persists, please report it on the discord (visit https://paxii.de/matix.html)",
            stringWriter.toString()
                    .replaceAll("\\r\\n", "<br>")
                    .replaceAll("\\t", "&emsp;")
    ));
  }

}
