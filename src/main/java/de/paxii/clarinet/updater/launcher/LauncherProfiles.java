package de.paxii.clarinet.updater.launcher;

import java.util.HashMap;

import lombok.Data;

@Data
public class LauncherProfiles {
  private HashMap<String, LauncherProfile> profiles;
  private String selectedProfile;
  private String clientToken;
  private HashMap<String, Authentication> authenticationDatabase;
  private HashMap<String, String> selectedUser;
  private HashMap<String, Object> settings;
  private LauncherVersion launcherVersion;
  private String analyticsToken;
  private int analyticsFailcount;
}
