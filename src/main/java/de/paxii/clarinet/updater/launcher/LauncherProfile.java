package de.paxii.clarinet.updater.launcher;

import lombok.Data;

@Data
public class LauncherProfile {
  private String name;
  private String type;
  private String created;
  private String lastUsed;
  private String lastVersionId;
  private String javaArgs;
}
