package de.paxii.clarinet.updater.launcher;

import java.util.HashMap;

import lombok.Data;

@Data
public class Authentication {
  private String accessToken;
  private String username;
  private HashMap<String, AuthenticationProfile> profiles;

  @Data
  class AuthenticationProfile {
    private String displayName;
  }
}
