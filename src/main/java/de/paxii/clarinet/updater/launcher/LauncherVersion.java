package de.paxii.clarinet.updater.launcher;

import lombok.Data;

@Data
public class LauncherVersion {
  private String name;
  private int format;
  private int profilesFormat;
}
