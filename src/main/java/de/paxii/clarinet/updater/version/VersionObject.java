package de.paxii.clarinet.updater.version;

import lombok.Getter;

/**
 * Created by Lars on 24.04.2017.
 */
public class VersionObject {
  @Getter
  private int clientBuild;
  @Getter
  private String gameVersion;
  @Getter
  private String url;
  @Getter
  private String directUrl;

  public VersionObject(int clientBuild, String gameVersion, String url, String directUrl) {
    this.clientBuild = clientBuild;
    this.gameVersion = gameVersion;
    this.url = url;
    this.directUrl = directUrl;
  }
}
