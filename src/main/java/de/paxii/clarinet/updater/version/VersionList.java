package de.paxii.clarinet.updater.version;

import java.util.ArrayList;
import java.util.Optional;

import lombok.Data;

/**
 * Created by Lars on 24.04.2017.
 */
@Data
public class VersionList {
  private ArrayList<VersionObject> versions = new ArrayList<>();

  public Optional<VersionObject> getByGameVersion(String gameVersion) {
    return versions.stream().filter(version -> version.getGameVersion().equals(gameVersion)).findFirst();
  }
}
